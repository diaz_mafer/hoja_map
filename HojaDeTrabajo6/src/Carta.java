

/**
 * Clase que implementa los metodos que se utilizan para realizar las operaciones
 * @author: María Fernanda López - 17160
 * @author: Paul Belches - 17088
 * @version: 6/03/18
 * Algoritmos y Estructura de Datos - seccion: 10
 */

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class Carta {
     private Map <String, String> mapa ;
     private Map <String, Integer> maso ;

    public Map<String, Integer> getMaso() {
        return maso;
    }

    public void setMaso(Map<String, Integer> maso) {
        this.maso = maso;
    }

     /**
     *Constructor de la clase
     * POST: Construye una nueva Calculadora
     */
    public Carta(int decision){               
        mapa = leerArchivo("./cards_desc.txt",decision);  
        maso = Factory.usarFactory(decision);
    }
    public Map<String, String> getMapa() {
        return mapa;
    }

    public void setMapa(Map<String, String> mapa) {
        this.mapa = mapa;
    }

  
    /** Método para operar expresiones en postfix
     * POST: el resultado de la operacion que se paso como parametro
     */
    public void agregar(String nombre){
        int v = 0;
        if (maso.containsKey(nombre)) v = maso.get(nombre);
        maso.put(nombre, v+1);
    }
    
    /**
     * Método para mostrar todos lo elementos que se encuentra en la implementación
     * del mapa que se decidio al usuario 
     * Solo se muestran cartas existentes
     */
    public void mostrar(){
        System.out.println("Las cartas que se tiene en general son: ");
        Iterator it = mapa.keySet().iterator();
        int cont = 0;
        while(it.hasNext() ){
            String key = (String) it.next();
            System.out.println("Nombre: " + key + " Tipo: "+ mapa.get(key));
            cont++;
        }
    }
    
    /**
     * Metodo para leer un archivo y pasar la informacion a una implementacion de mapa
     * @param cadena de cartas
     * @param descision implementacion de mapa que se utilizara
     * @return 
     */   
    
    public Map leerArchivo(String cadena, int descision) {
        mapa = Factory.usarFactory(descision);
        File f;
        FileReader fr;
        BufferedReader br;

        String postFix = "";

       //Este bloque de codigo tiene como objetivo leer la cadena de texto que
       //el usuario haya establecido previamente
        try {

            f = new File (cadena);            
            fr = new FileReader(f);
            br = new BufferedReader(fr);      
            
            String linea;

            while( (linea = br.readLine()) != null) {
                String sep = Pattern.quote("|");
                String[] partes = linea.split(sep);
                mapa.put(partes[0],partes[1]); 
            }                                            
            
            br.close();
            fr.close();

        }
        //Si el archivo se modifica manualmente o sucede cualquier otros tipo de
        //error, este sera comunicado con el usuario
        catch (Exception e) {

            System.err.println("Se produjo un error: " + e);                 

        }                

        return mapa;

    }
}


