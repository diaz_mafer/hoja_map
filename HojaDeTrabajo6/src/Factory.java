
/**
 * Clase que define el patron de diseno Factory
* @author: María Fernanda López - 17160
 * @author: Paul Belches - 17088
 * @version: 16/02/18
 * Algoritmos y Estructura de Datos - seccion: 10
 */


import java.util.*;
public class Factory {
    public static Map usarFactory(int criteria){
        if (criteria == 1) return new HashMap();
        if (criteria == 2) return new TreeMap();
        else return new LinkedHashMap(); 
    }
}