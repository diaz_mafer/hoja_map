/**
 * Clase principal que se comunica con el usuario.
 * @author: María Fernanda López - 17060
 * @author: Paul Belches - 17088
 * @version: 16/02/18
 * Algoritmos y Estructura de Datos - seccion: 10
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class HojaDeTrabajo6 {

    public static void main(String[] args) {
        
        Carta maso;
        Scanner teclado = new Scanner(System.in);
        boolean salir = true ;
        int decision;
        double resultado;     
        
        System.out.println("\nBienvenido a la Cartaton!\n");
        
        System.out.println("Ingrese la implementacion a usar:\n");  
        System.out.println("1. Hashmap");
        System.out.println("2. Treemap");
        System.out.println("3. Linkedhasmap");
        
        decision = teclado.nextInt();
        
        //Linea de codigo que lee inmediatamente el archivo .txt
        maso = new Carta(decision);
                
        while (salir) {
            System.out.println("\n____________________________________");            
            System.out.println("Ingrese la accion que desea realizar:\n");  
            System.out.println("1. Agregar carta");
            System.out.println("2. Mostrar tipo de carta");
            System.out.println("3. Mostrar colleccion de carta");
            System.out.println("4. Mostrar por tipo");
            System.out.println("5. Mostrar nombre y tipo de cartas");
            System.out.println("6. Mostrar nombre y tipo de cartas (ordenadas por tipo)");
            System.out.println("Exit");
            
            decision = teclado.nextInt();
            String n;
            switch (decision){
                
                case 1: 
                    System.out.println("Ingrese el nombre de la carta"); 
                    n = teclado.nextLine();
                    n = teclado.nextLine();
                    if (maso.getMapa().containsKey(n)) {
                        maso.agregar(n);
                    } else {
                       System.out.println("Esa carta no existe dentro de la collecion");
                    }   
                break;
                case 2:
                    System.out.println("Ingrese el nombre de la carta"); 
                    n = teclado.nextLine();
                    n = teclado.nextLine();
                    if (maso.getMapa().containsKey(n)) {
                       System.out.println("El tipo de esta carta es "+maso.getMapa().get(n));
                    } else {
                       System.out.println("Esta carta no existe dentro del maso");
                    } 
                break;
                case 3:
                    System.out.println("Las cartas que tiene en su maso son: ");
                    for (Map.Entry<String,Integer> entry: maso.getMaso().entrySet()){
                       System.out.println(entry.getKey());
                       System.out.println("Tipo: "+maso.getMapa().get(entry.getKey()));
                       System.out.println(entry.getValue().toString() + " carta(s)");
                    }
                break; 
                case 5:
                    maso.mostrar();
                                        
                break;
                case 4:
                    System.out.println("Sus cartas ordenadas por tipo son: ");
                    
                    System.out.println("Tipo: Monstruo");
                    for (Map.Entry<String,Integer> entry: maso.getMaso().entrySet()){
                        String tipo = maso.getMapa().get(entry.getKey());
                        if(tipo.equals("Monstruo")){
                            System.out.println(entry.getKey() + " " + entry.getValue().toString() + " carta(s)");                            
                        }
                        if(tipo.equals("Trampa")){                           
                            System.out.println("Tipo: Trampa");
                            System.out.println(entry.getKey() + " " + entry.getValue().toString() + " carta(s)");                                                 
                        }                       
                        if(tipo.equals("Hechizo")){                            
                            System.out.println("Tipo: Hechizo");
                            System.out.println(entry.getKey() + " " + entry.getValue().toString() + " carta(s)");                  
                        }              
                    }             
                break;
                case 6:
                    Iterator itt = maso.getMapa().keySet().iterator();
                    Iterator itt2 = maso.getMapa().keySet().iterator();
                    Iterator itt3 = maso.getMapa().keySet().iterator();
                    
                    System.out.println();
                    System.out.println("Tipo: Monstruo");
                    while(itt.hasNext()){
                      String key = (String) itt.next();
                      String type = maso.getMapa().get(key);
                      if (type.equals("Monstruo")){
                        System.out.println("Nombre: " + key );
                      }
                    }
                    System.out.println();
                    System.out.println("Tipo: Hechizo");
                    while(itt2.hasNext()){
                      String key = (String) itt2.next();
                      String type = maso.getMapa().get(key);
                      if (type.equals("Hechizo")){
                        System.out.println("Nombre: " + key + " type: " +  maso.getMapa().get(key));
                      }
                    }
                    System.out.println();
                    System.out.println("Tipo: Trampa");
                    while(itt3.hasNext()){
                      String key = (String) itt3.next();
                      String type = maso.getMapa().get(key);
                      if (type.equals("Trampa")){
                        System.out.println("Nombre: " + key + " type: " +  maso.getMapa().get(key));
                      }
                    }
                    
                break;
            default:
                salir = false;
            break;
            }
    
                                      
        }
    }
        
    }    


